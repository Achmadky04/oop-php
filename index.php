<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$kambing = new binatang('Shaun');
echo "Name : " .$kambing->name . "<br>";
echo "Legs : " .$kambing->legs . "<br>";
echo "Cold Blooded : " .$kambing->cold_blooded . "<br> <br>";


$kodok = new frog('Buduk');

echo "Name : " .$kodok->name . "<br>";
echo "Legs : " .$kodok->legs . "<br>";
echo "Cold Blooded : " .$kodok->cold_blooded . "<br>";
echo $kodok->Jump() . "<br><br>";


$sungokong = new ape('Kera Sakti');

echo "Name : " .$sungokong->name . "<br>";
echo "Legs : " .$sungokong->legs . "<br>";
echo "Cold Blooded : " .$sungokong->cold_blooded . "<br>";
echo $sungokong->Yell();

?>